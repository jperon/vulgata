#!/usr/bin/perl
# 31/3/04 - little.mouth@soon.com - GPL
# Perl script to generate LaTeX files from VulSearch Latin source files
# Requirements: perl, sed
#
# Usage: perl makemarkdown.pl
#
# Output:
# files latex/ABBREV.tex for each abbreviation ABBREV of a book of the Bible
#
# Background and documentation at http://vulsearch.sf.net/plain.html
# The main bulk of the conversion is done using makelatex.sed: this script
# just adds a header, and deals with a couple of eccentricities (see inline
# comments passim).

$source='source'; #source directory
$ext='lat'; #file extension
$md='markdown'; #MarkDown output directory

#initialize our hash of book names
@bib =
(
  'Gn', 'Ex', 'Lv', 'Nm', 'Dt', 'Jos', 'Jdc', 'Rt', '1Rg', '2Rg', '3Rg', '4Rg', '1Par', '2Par',
  'Esr', 'Neh', 'Tob', 'Jdt', 'Est', 'Job', 'Ps', 'Pr', 'Ecl', 'Ct', 'Sap', 'Sir', 'Is', 'Jr',
  'Lam', 'Bar', 'Ez', 'Dn', 'Os', 'Joel', 'Am', 'Abd', 'Jon', 'Mch', 'Nah', 'Hab', 'Soph', 'Agg',
  'Zach', 'Mal', '1Mcc', '2Mcc', 'Mt', 'Mc', 'Lc', 'Jo', 'Act', 'Rom', '1Cor', '2Cor', 'Gal',
  'Eph', 'Phlp', 'Col', '1Thes', '2Thes', '1Tim', '2Tim', 'Tit', 'Phlm', 'Hbr', 'Jac', '1Ptr',
  '2Ptr', '1Jo', '2Jo', '3Jo', 'Jud', 'Apc'
);

#for special characters in the book names
sub cleanup
{
  my ($s)=@_;
  $s =~ s/\s/_/g;
  return $s;
}

foreach my $book (@bib)
{
  open(DATA, "sed -n -e \"/^$book\\//p\" $source/data.txt |")    || die "can't fork sed";
  ($_,$long,$short,$create,$proof)=split /\//, <DATA>;
  close DATA;
  $short=cleanup($short);
  chomp($proof);

  open(OUT,"> $md/$book.md") || die "can't open $md/$book.md";

  #print head
  print OUT "# $long {#$short}\n";

  #print main body of the book, mostly converted by a sed script
  open(IN, "sed -f makemarkdown.sed $source/$book.$ext |")    || die "can't fork sed";
  $x=<IN>;
  chomp($x);
  print OUT "$x\n";

  print OUT "$x" while ($x=<IN>);
  close(IN);

  close(OUT);
}
