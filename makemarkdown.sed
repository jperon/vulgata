#!/usr/bin/sed -f
# 10/1/05 - little.mouth@soon.com - GPL
# sed script to generate LaTeX markup from from VulSearch Latin source files. An
# accompanying Perl script, makelatex.pl, adds headers and footers to make a
# LaTeX file suitable for includsion in latex/{vulgate,twocolumn}.tex
# To convert the resulting source files to PDF, you can use the accompanying
# batch files makepdf-vulgate.bat and makepdf-twocolumn.bat
# which are also valid as bash scripts.
#
# Requirements: sed
#
# Usage: sed -f makelatex.sed SOURCEFILE where SOURCEFILE is the Latin source
# file to convert
#
# Output: source file with appropriate LaTeX markup added
#
# Background and documentation at http://vulsearch.sf.net/plain.html including a
# description of the source format (see also comments below).


#first take care of \
s/\\/@/g

#===
#verse numbering
#first move [ at start of verse text to start of line
s/\([1-9][0-9:]* \)\[/[\1/

#now do chapter numbers
s/\(\[*\)\([0-9][0-9]*\):1 /\n## \2\n\n\1/

#finally do the other verses
s/\(\[*\)\([0-9][0-9]*\):\([0-9][0-9]*\) /\1^\3^ /
#===

#poetry [ ] /
s/\[/\n/g
s/\]@*/\n@/g
s/\//\\\n/g

#do < >
s/</\[/g
s/>/.\]{style="font-variant:small-caps"} /g

#finally do the orginal \ (which have become @)
s/@/\
\
/g
